module gitlab.com/abhishp/sudoku_solver

go 1.12

require (
	github.com/fatih/color v1.7.0 // indirect
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/rakyll/gotest v0.0.0-20180125184505-86f0749cd8cc // indirect
	github.com/stretchr/testify v1.3.0
	github.com/yourbasic/bit v0.0.0-20180313074424-45a4409f4082
)
