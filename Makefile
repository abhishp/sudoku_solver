APP_EXECUTABLE="./out/sudoku_solver"
ALL_PACKAGES=$(shell go list ./...)

help:
	@echo "make test - runs static code check tools & runs tests"
	@echo "make build - creates the executable"

vet: ## Run the vet tool
	GO111MODULE=on go vet $(ALL_PACKAGES)

fmt:
	GO111MODULE=on go fmt $(ALL_PACKAGES)

clean:
	GO111MODULE=on go clean
	rm -rf out/

build: clean
	mkdir -p out
	GO111MODULE=on go build -o $(APP_EXECUTABLE)

test:
	GO111MODULE=on go get github.com/rakyll/gotest
	GO111MODULE=on gotest $(ALL_PACKAGES) -p=1
