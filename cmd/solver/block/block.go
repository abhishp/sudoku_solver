package block

import (
	"github.com/yourbasic/bit"
	"gitlab.com/abhishp/sudoku_solver/cmd/solver/constants"
)

var possibleValues = []int{1, 2, 3, 4, 5, 6, 7, 8, 9}

type Block interface {
	Add(int)
	GetPossibilities() *bit.Set
	Reset(int)
}

type block struct {
	possibilities *bit.Set
}

func NewBlock() Block {
	return &block{
		possibilities: bit.New(constants.PossibleCellValues...),
	}
}

func (b *block) Add(value int) {
	b.possibilities = b.possibilities.Delete(value)
}

func (b *block) GetPossibilities() *bit.Set {
	return b.possibilities
}

func (b *block) Reset(value int) {
	b.possibilities = b.possibilities.Add(value)
}
