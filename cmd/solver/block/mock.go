package block

import (
	"github.com/stretchr/testify/mock"
	"github.com/yourbasic/bit"
)

type MockBlock struct {
	mock.Mock
}

func (mb MockBlock) Add(value int) {
	mb.Called(value)
}

func (mb MockBlock) GetPossibilities() *bit.Set {
	args := mb.Called()
	if args.Get(0) == nil {
		return nil
	}
	return args.Get(0).(*bit.Set)
}

func (mb MockBlock) Reset(value int) {
	mb.Called(value)
}
