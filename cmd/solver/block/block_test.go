package block

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBlockCreation(t *testing.T) {
	b := NewBlock()

	assert.IsType(t, &block{}, b)
	assert.Equal(t, "{1..9}", b.GetPossibilities().String())
}

func TestBlock_Add_ShouldRemoveGivenValueFromPossibilities(t *testing.T) {
	b := NewBlock()
	assert.Equal(t, "{1..9}", b.GetPossibilities().String())

	b.Add(1)

	assert.Equal(t, "{2..9}", b.GetPossibilities().String())
}

func TestBlock_GetPossibilities_ShouldReturnThePossibilitiesForTheBlock(t *testing.T) {
	b := NewBlock()
	assert.Equal(t, "{1..9}", b.GetPossibilities().String())

	b.Add(1)
	b.Add(5)
	b.Add(7)

	assert.Equal(t, "{2..4 6 8 9}", b.GetPossibilities().String())
}

func TestBlock_Reset_ShouldAddGivenValueToPossibilities(t *testing.T) {
	b := NewBlock()
	b.Add(1)
	b.Add(5)
	b.Add(7)
	assert.Equal(t, "{2..4 6 8 9}", b.GetPossibilities().String())

	b.Reset(5)

	assert.Equal(t, "{2..6 8 9}", b.GetPossibilities().String())

	b.Reset(1)

	assert.Equal(t, "{1..6 8 9}", b.GetPossibilities().String())

}