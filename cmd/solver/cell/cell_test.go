package cell

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"github.com/yourbasic/bit"
	"gitlab.com/abhishp/sudoku_solver/cmd/solver/block"
	test "gitlab.com/abhishp/sudoku_solver/cmd/solver/testing"
)

type CellSuite struct {
	suite.Suite
	mockRow    block.MockBlock
	mockColumn block.MockBlock
	mockBox    block.MockBlock
}

func (suite *CellSuite) SetupTest() {
	suite.mockBox = block.MockBlock{}
	suite.mockColumn = block.MockBlock{}
	suite.mockRow = block.MockBlock{}
}

func TestCellSuite(t *testing.T) {
	suite.Run(t, new(CellSuite))
}

func (suite *CellSuite) getCell() *Cell {
	return NewCell(suite.mockRow, suite.mockColumn, suite.mockBox)
}

func (suite *CellSuite) mockGetPossibilityCalls() {
	suite.mockRow.On("GetPossibilities").Once().Return(bit.New(1, 2, 3, 4))
	suite.mockColumn.On("GetPossibilities").Once().Return(bit.New(2, 3, 4))
	suite.mockBox.On("GetPossibilities").Once().Return(bit.New(2, 4))
}

func (suite *CellSuite) mockUpdateCalls(value int) {
	suite.mockRow.On("Add", value).Once()
	suite.mockColumn.On("Add", value).Once()
	suite.mockBox.On("Add", value).Once()
}

func (suite *CellSuite) mockResetCalls(value int) {
	suite.mockRow.On("Reset", value).Once()
	suite.mockColumn.On("Reset", value).Once()
	suite.mockBox.On("Reset", value).Once()
}

func (suite *CellSuite) TestNewCell() {
	cell := suite.getCell()

	assert.NotNil(suite.T(), cell)
	assert.IsType(suite.T(), &Cell{}, cell)
	assert.False(suite.T(), cell.solved)
}

func (suite *CellSuite) Test_Init_ShouldReturnErrorWhenValueIsInvalid() {
	cell := suite.getCell()

	test.AssertErrorWithMessage(suite.T(), cell.Init(-1), "invalid value for cell")
	test.AssertErrorWithMessage(suite.T(), cell.Init(10), "invalid value for cell")
}

func (suite *CellSuite) Test_Init_ShouldReturnErrorWhenValueIsPresentInSameBlock() {
	suite.mockGetPossibilityCalls()

	cell := suite.getCell()

	test.AssertErrorWithMessage(suite.T(), cell.Init(1), "invalid value for cell")
}

func (suite *CellSuite) Test_Init_ShouldTriggerUpdatesOnBlocksForNonZeroValue() {
	suite.mockGetPossibilityCalls()
	suite.mockUpdateCalls(2)
	cell := suite.getCell()

	assert.NoError(suite.T(), cell.Init(2))

	assert.Equal(suite.T(), uint8(2), cell.Value)
	assert.True(suite.T(), cell.solved)
}

func (suite *CellSuite) Test_Init_ShouldNotTriggerUpdatesOnBlocksForZeroValue() {
	suite.mockGetPossibilityCalls()
	cell := suite.getCell()

	assert.NoError(suite.T(), cell.Init(0))

	assert.Equal(suite.T(), uint8(0), cell.Value)
	assert.False(suite.T(), cell.solved)
}

func (suite *CellSuite) Test_Init_ShouldSetLastValueForZeroValue() {
	suite.mockGetPossibilityCalls()
	cell := suite.getCell()

	assert.NoError(suite.T(), cell.Init(0))

	assert.Equal(suite.T(), 0, cell.lastValue)
}

func (suite *CellSuite) Test_CanAdd_ShouldReturnFalseWhenSolved() {
	suite.mockGetPossibilityCalls()
	suite.mockUpdateCalls(2)
	cell := suite.getCell()
	cell.Init(2)

	assert.False(suite.T(), cell.CanAdd(1))
}

func (suite *CellSuite) Test_CanAdd_ShouldReturnFalseWhenNotPossibleToAddTheValue() {
	suite.mockGetPossibilityCalls()
	cell := suite.getCell()

	assert.NoError(suite.T(), cell.Init(0))
	assert.False(suite.T(), cell.CanAdd(1))
}

func (suite *CellSuite) Test_CanAdd_ShouldReturnTrueWhenPossibleToAddTheValue() {
	suite.mockGetPossibilityCalls()
	cell := suite.getCell()

	assert.NoError(suite.T(), cell.Init(0))
	assert.True(suite.T(), cell.CanAdd(2))
}

func (suite *CellSuite) Test_GetNextPossibility_ShouldReturnTheNextPossibilityGreaterThanLastValue() {
	suite.mockGetPossibilityCalls()
	cell := suite.getCell()
	cell.Init(0)

	possibility, ok := cell.GetNextPossibility()

	assert.Equal(suite.T(), 2, possibility)
	assert.True(suite.T(), ok)
}

func (suite *CellSuite) Test_GetNextPossibility_ShouldReturnFalseWhenNoPossibilityIsLeft() {
	suite.mockGetPossibilityCalls()
	cell := suite.getCell()
	cell.Init(0)
	cell.lastValue = 4

	possibility, ok := cell.GetNextPossibility()

	assert.Zero(suite.T(), possibility)
	assert.False(suite.T(), ok)
}

func (suite *CellSuite) Test_IsSolved_ShouldReturnTrueWhenMarkedAsSolved() {
	cell := suite.getCell()
	cell.solved = true

	assert.True(suite.T(), cell.IsSolved())
}

func (suite *CellSuite) Test_IsSolved_ShouldReturnFalseWhenValueIsZero() {
	cell := suite.getCell()
	cell.Init(0)

	assert.False(suite.T(), cell.IsSolved())
}

func (suite *CellSuite) Test_IsSolved_ShouldReturnTrueWhenValueIsNonZero() {
	suite.mockGetPossibilityCalls()
	suite.mockUpdateCalls(2)
	cell := suite.getCell()
	cell.Init(2)

	assert.True(suite.T(), cell.IsSolved())
}

func (suite *CellSuite) Test_Set_ShouldThrowErrorIfTheValueCannotBeAdded() {
	suite.mockGetPossibilityCalls()
	cell := suite.getCell()
	cell.Init(0)

	test.AssertErrorWithMessage(suite.T(), cell.Set(1), "cannot set cell to 1")
}

func (suite *CellSuite) Test_Set_ShouldThrowErrorForASolvedCell() {
	cell := suite.getCell()
	cell.solved = true

	test.AssertErrorWithMessage(suite.T(), cell.Set(1), "cannot set cell to 1")
}

func (suite *CellSuite) Test_Set_ShouldSetTheValueAndTriggerUpdates() {
	suite.mockGetPossibilityCalls()
	suite.mockUpdateCalls(4)
	cell := suite.getCell()
	cell.Init(0)

	err := cell.Set(4)

	assert.NoError(suite.T(), err)
	assert.Equal(suite.T(), uint8(4), cell.Value)
}

func (suite *CellSuite) Test_Set_ShouldSetUpdateTheLastValueToPreviousValue() {
	suite.mockGetPossibilityCalls()
	suite.mockGetPossibilityCalls()
	suite.mockUpdateCalls(4)
	suite.mockUpdateCalls(2)
	cell := suite.getCell()
	cell.Init(0)

	cell.Set(4)
	assert.Equal(suite.T(), 0, cell.lastValue)

	cell.Set(2)
	assert.Equal(suite.T(), 4, cell.lastValue)
}

func (suite *CellSuite) Test_Unset_ShouldThrowErrorForASolvedCell() {
	cell := suite.getCell()
	cell.solved = true

	test.AssertErrorWithMessage(suite.T(), cell.Unset(), "cannot unset a solved cell")
}

func (suite *CellSuite) Test_Unset_ShouldSetValueToZeroAndTriggerResetOnBlocks() {
	suite.mockResetCalls(4)
	cell := suite.getCell()
	cell.Value = uint8(4)

	err := cell.Unset()

	assert.NoError(suite.T(), err)
	assert.Equal(suite.T(), uint8(0), cell.Value)
}

func (suite *CellSuite) Test_Unset_ShouldUpdateTheLastValue() {
	suite.mockResetCalls(4)
	cell := suite.getCell()
	cell.lastValue = 2
	cell.Value = uint8(4)

	cell.Unset()

	assert.Equal(suite.T(), 4, cell.lastValue)
}
func (suite *CellSuite) Test_Reset_ShouldThrowErrorForASolvedCell() {
	cell := suite.getCell()
	cell.solved = true

	test.AssertErrorWithMessage(suite.T(), cell.Reset(), "cannot unset a solved cell")
}

func (suite *CellSuite) Test_Reset_ShouldSetValueToZeroAndTriggerResetOnBlocks() {
	suite.mockResetCalls(4)
	cell := suite.getCell()
	cell.Value = uint8(4)

	err := cell.Reset()

	assert.NoError(suite.T(), err)
	assert.Equal(suite.T(), uint8(0), cell.Value)
}

func (suite *CellSuite) Test_Reset_ShouldUpdateTheLastValueToZero() {
	suite.mockResetCalls(4)
	cell := suite.getCell()
	cell.lastValue = 2
	cell.Value = uint8(4)

	cell.Reset()

	assert.Equal(suite.T(), 0, cell.lastValue)
}

func (suite *CellSuite) Test_String_ShouldReturnValueForSolvedCells() {
	cell := suite.getCell()
	cell.Value = uint8(4)

	assert.Equal(suite.T(), "4", cell.String())
}

func (suite *CellSuite) Test_String_ShouldReturnPossibilitiesForUnSolvedCell() {
	suite.mockGetPossibilityCalls()
	cell := suite.getCell()
	cell.Init(0)

	assert.Equal(suite.T(), "{2 4}", cell.String())
}
