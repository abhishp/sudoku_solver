package cell

import (
	"errors"
	"strconv"

	"github.com/yourbasic/bit"
	"gitlab.com/abhishp/sudoku_solver/cmd/solver/block"
	"gitlab.com/abhishp/sudoku_solver/cmd/solver/constants"
)

type Cell struct {
	Value     uint8
	solved    bool
	row       block.Block
	column    block.Block
	box       block.Block
	lastValue int
}

func NewCell(row, column, box block.Block) *Cell {
	return &Cell{
		row:    row,
		column: column,
		box:    box,
	}
}

func (c *Cell) Init(value int) error {
	if value < 0 || value > constants.BoardSize {
		return errors.New("invalid value for cell")
	}
	if value > 0 && !c.CanAdd(value) {
		return errors.New("invalid value for cell")
	}
	c.Value = uint8(value)
	c.solved = value > 0 && value <= constants.BoardSize
	if c.IsSolved() {
		c.triggerUpdate()
	} else {
		c.lastValue = 0
	}

	return nil
}

func (c *Cell) CanAdd(value int) bool {
	return !c.solved && c.getPossibilities().Contains(value)
}

func (c Cell) GetNextPossibility() (int, bool) {
	if possibility := c.getPossibilities().Next(c.lastValue); possibility != -1 {
		return possibility, true
	}
	return 0, false
}

func (c *Cell) IsSolved() bool {
	return c.solved || (c.Value > 0 && c.Value <= constants.BoardSize)
}

func (c *Cell) Reset() error {
	err := c.Unset()
	if err != nil {
		return err
	}
	c.lastValue = 0
	return nil
}

func (c *Cell) Set(value int) error {
	if !c.CanAdd(value) {
		return errors.New("cannot set cell to " + strconv.Itoa(value))
	}
	c.lastValue = int(c.Value)
	c.Value = uint8(value)
	c.triggerUpdate()
	return nil
}

func (c Cell) String() string {
	if c.IsSolved() {
		return strconv.Itoa(int(c.Value))
	}

	return c.getPossibilities().String()
}

func (c *Cell) Unset() error {
	if c.solved {
		return errors.New("cannot unset a solved cell")
	}
	c.triggerReset()
	c.lastValue = int(c.Value)
	c.Value = 0
	return nil
}
func (c Cell) triggerReset() {
	if c.Value == 0 {
		return
	}
	value := int(c.Value)
	c.row.Reset(value)
	c.column.Reset(value)
	c.box.Reset(value)
}

func (c *Cell) triggerUpdate() {
	if c.Value == 0 {
		return
	}
	value := int(c.Value)
	c.row.Add(value)
	c.column.Add(value)
	c.box.Add(value)
}

func (c *Cell) getPossibilities() *bit.Set {
	return c.row.GetPossibilities().
		And(c.column.GetPossibilities()).
		And(c.box.GetPossibilities())
}
