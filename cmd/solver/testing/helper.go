package testing

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func AssertErrorWithMessage(t *testing.T, error error, msg string) {
	assert.Error(t, error, )
	assert.Equal(t, msg, error.Error())
}
