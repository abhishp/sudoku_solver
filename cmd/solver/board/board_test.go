package board

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/abhishp/sudoku_solver/cmd/solver/cell"
	test "gitlab.com/abhishp/sudoku_solver/cmd/solver/testing"
)

type BoardSuite struct {
	suite.Suite
	board          Board
	puzzle         [][]int
	puzzleSolution [][]int
}

func (suite *BoardSuite) SetupTest() {
	suite.puzzle = [][]int{
		{0, 0, 0, 0, 0, 0, 5, 2, 0},
		{0, 0, 0, 0, 0, 2, 0, 0, 7},
		{0, 4, 0, 0, 6, 8, 0, 0, 0},
		{0, 0, 8, 0, 0, 0, 6, 0, 2},
		{0, 0, 0, 0, 1, 0, 0, 0, 0},
		{0, 1, 0, 0, 0, 5, 0, 0, 0},
		{0, 3, 9, 0, 5, 0, 0, 0, 0},
		{5, 0, 6, 4, 0, 3, 0, 0, 0},
		{7, 0, 0, 9, 0, 0, 0, 3, 0}}

	suite.puzzleSolution = [][]int{
		{8, 6, 3, 1, 9, 7, 5, 2, 4},
		{1, 9, 5, 3, 4, 2, 8, 6, 7},
		{2, 4, 7, 5, 6, 8, 1, 9, 3},
		{9, 5, 8, 7, 3, 4, 6, 1, 2},
		{6, 7, 4, 2, 1, 9, 3, 5, 8},
		{3, 1, 2, 6, 8, 5, 7, 4, 9},
		{4, 3, 9, 8, 5, 1, 2, 7, 6},
		{5, 2, 6, 4, 7, 3, 9, 8, 1},
		{7, 8, 1, 9, 2, 6, 4, 3, 5}}

	suite.board, _ = NewBoard(suite.puzzle)
}

func getCell(b Board, row, col int) *cell.Cell {
	return b.(*board).cells[row][col]
}

func TestBoardSuite(t *testing.T) {
	suite.Run(t, new(BoardSuite))
}

func (suite *BoardSuite) TestNewBoard_ShouldValidateNumberOfRows() {
	b, error := NewBoard([][]int{})

	assert.Nil(suite.T(), b)
	test.AssertErrorWithMessage(suite.T(), error, "invalid number of rows")
}

func (suite *BoardSuite) TestNewBoard_ShouldValidateNumberOfColumnsForEachRow() {
	b, error := NewBoard([][]int{{1, 0, 0, 0, 0, 0, 0, 0, 0}, {}, {}, {}, {}, {}, {}, {}, {}})

	assert.Nil(suite.T(), b)
	test.AssertErrorWithMessage(suite.T(), error, "invalid number of columns in row 2")
}

func (suite *BoardSuite) TestNewBoard_ShouldValidateCellValues() {
	b, error := NewBoard([][]int{{10, 0, 0, 0, 0, 0, 0, 0, 0}, {}, {}, {}, {}, {}, {}, {}, {}})

	assert.Nil(suite.T(), b)
	test.AssertErrorWithMessage(suite.T(), error, "invalid value for cell at row: 0, column: 0")
}

func (suite *BoardSuite) TestNewBoard_ShouldValidatePuzzleValues() {
	puzzle := suite.puzzle
	puzzle[0][0], puzzle[0][1] = 1, 1

	b, error := NewBoard(puzzle)

	assert.Nil(suite.T(), b)
	test.AssertErrorWithMessage(suite.T(), error, "invalid puzzle (cell value duplicate at: (1,2)")
}

func (suite *BoardSuite) TestNewBoard() {
	b, error := NewBoard(suite.puzzle)

	assert.IsType(suite.T(), &board{}, b)
	assert.Nil(suite.T(), error)

	for idxRow, row := range suite.puzzle {
		for idxCol, val := range row {
			assert.Equal(suite.T(), uint8(val), getCell(b, idxRow, idxCol).Value)
		}
	}
}

func (suite *BoardSuite) Test_GetNextResolved_ShouldReturnNextUnsolvedCell() {
	b := suite.board

	cell, ok := b.GetNextUnsolved()

	assert.True(suite.T(), ok)
	assert.Equal(suite.T(), getCell(b, 0, 0), cell)

	cell.Set(1)
	cell, ok = b.GetNextUnsolved()

	assert.True(suite.T(), ok)
	assert.Equal(suite.T(), getCell(b, 0, 1), cell)
}

func (suite *BoardSuite) Test_IsSolved_ShouldReturnFalseWhenAtLeastOneCellIsUnsolved() {
	puzzle := suite.puzzleSolution
	puzzle[0][1] = 0
	b, _ := NewBoard(puzzle)

	assert.False(suite.T(), b.IsSolved())
}

func (suite *BoardSuite) Test_IsSolved_ShouldReturnTrueWhenPuzzleIsSolved() {
	b, _ := NewBoard(suite.puzzleSolution)

	assert.True(suite.T(), b.IsSolved())
}
