package board

import (
	"errors"
	"strconv"
	"strings"

	"gitlab.com/abhishp/sudoku_solver/cmd/solver/block"
	"gitlab.com/abhishp/sudoku_solver/cmd/solver/cell"
	"gitlab.com/abhishp/sudoku_solver/cmd/solver/constants"
)

type Board interface {
	GetNextUnsolved() (*cell.Cell, bool)
	IsSolved() bool
	String() string
}

type board struct {
	cells   [constants.BoardSize][constants.BoardSize]*cell.Cell
	rows    [constants.BoardSize]block.Block
	columns [constants.BoardSize]block.Block
	boxes   [constants.BoardSize]block.Block
	errors  chan error
}

func NewBoard(puzzle [][]int) (Board, error) {
	b := board{
		rows:    createBlocks(),
		columns: createBlocks(),
		boxes:   createBlocks(),
		errors:  make(chan error),
	}
	if len(puzzle) != constants.BoardSize {
		return nil, errors.New("invalid number of rows")
	}
	for r, row := range puzzle {
		if len(row) != constants.BoardSize {
			return nil, errors.New("invalid number of columns in row " + strconv.Itoa(r+1))
		}
		for c, val := range row {
			boxIdx := getBoxIndex(r, c)
			if val < 0 || val > constants.BoardSize {
				return nil, errors.New("invalid value for cell at row: " + strconv.Itoa(r) + ", column: " + strconv.Itoa(c))
			}
			b.cells[r][c] = cell.NewCell(b.rows[r], b.columns[c], b.boxes[boxIdx])
		}
	}
	for r, row := range puzzle {
		for c, value := range row {
			if err := b.cells[r][c].Init(value); err != nil {
				return nil, errors.New("invalid puzzle (cell value duplicate at: (" + strconv.Itoa(r+1) + "," + strconv.Itoa(c+1) + ")")
			}
		}
	}

	return &b, nil
}

func createBlocks() [constants.BoardSize]block.Block {
	return [constants.BoardSize]block.Block{
		block.NewBlock(),
		block.NewBlock(),
		block.NewBlock(),
		block.NewBlock(),
		block.NewBlock(),
		block.NewBlock(),
		block.NewBlock(),
		block.NewBlock(),
		block.NewBlock()}
}

func (b *board) GetNextUnsolved() (*cell.Cell, bool) {
	for _, row := range b.cells {
		for _, cell := range row {
			if !cell.IsSolved() {
				return cell, true
			}
		}
	}
	return nil, false
}

func (b *board) IsSolved() bool {
	for _, row := range b.cells {
		for _, cell := range row {
			if !cell.IsSolved() {
				return false
			}
		}
	}
	return true
}

func (b *board) String() string {
	rows := make([]string, constants.BoardSize)
	for idx, row := range b.cells {
		rows[idx] = getRowString(row)
		if idx < constants.BoardSize-1 && idx%constants.BoxSize+1 == constants.BoxSize {
			rows[idx] += "\n" + getSeparatorRow(rows[idx])
		}
	}
	return strings.Join(rows, "\n")
}

func getBoxIndex(r, c int) int {
	row, col := r/constants.BoxSize, c/constants.BoxSize
	return row*constants.BoxSize + col
}

func getSeparatorRow(row string) string {
	parts := strings.Split(row, "|")
	for i := 0; i < len(parts); i++ {
		parts[i] = strings.Repeat("-", len(parts[i]))
	}
	separator := strings.Join(parts, "|")
	return separator
}

func getRowString(row [9]*cell.Cell) (rowString string) {
	for i, cell := range row {
		rowString += cell.String()
		if i == constants.BoardSize {
			return
		}
		if i%constants.BoxSize == constants.BoxSize-1 {
			rowString += " | "
		} else {
			rowString += ","
		}
	}
	return rowString
}
