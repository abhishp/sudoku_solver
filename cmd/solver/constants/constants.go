package constants

const (
	BoardSize = 9
	BoxSize = 3
)

var PossibleCellValues = []int{1, 2, 3, 4, 5, 6, 7, 8, 9}